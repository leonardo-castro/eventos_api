package com.eventos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.entity.Cidade;
import com.eventos.model.service.CidadeService;

@RestController
public class CidadesController {

	@Autowired
	private CidadeService cidadeService;

	/**
	 * CRUD OPERATIONS
	 */
	@RequestMapping(value = "/cidades/{id}", method = RequestMethod.GET, produces = "application/json")
	public Cidade show(@PathVariable Long id) throws Exception {
		return cidadeService.show(id);
	}

	@RequestMapping(value = "/cidades", method = RequestMethod.GET, produces = "application/json")
	public List<Cidade> index() {
		return cidadeService.index();
	}

	@RequestMapping(value = "/cidades", method = RequestMethod.POST, consumes = "application/json")
	public String create(@RequestBody Cidade cidade) throws Exception {
		cidadeService.create(cidade);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/cidades/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public String update(@RequestBody Cidade cidade, @PathVariable Long id) throws Exception {
		cidadeService.update(cidade, id);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/cidades/{id}", method = RequestMethod.DELETE)
	public String destroy(@PathVariable Long id) {
		cidadeService.destroy(id);
		return HttpStatus.OK.toString();
	}

	/**
	 * EXCEPTION HANDLER
	 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

}
