package com.eventos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.entity.Endereco;
import com.eventos.model.service.EnderecoService;

@RestController
public class EnderecosController {

	@Autowired
	private EnderecoService enderecoService;

	/**
	 * CRUD OPERATIONS
	 */
	@RequestMapping(value = "/enderecos/{id}", method = RequestMethod.GET, produces = "application/json")
	public Endereco show(@PathVariable Long id) throws Exception {
		return enderecoService.show(id);
	}

	@RequestMapping(value = "/enderecos", method = RequestMethod.GET, produces = "application/json")
	public List<Endereco> index() {
		return enderecoService.index();
	}

	@RequestMapping(value = "/enderecos", method = RequestMethod.POST, consumes = "application/json")
	public String create(@RequestBody Endereco endereco) throws Exception {
		enderecoService.create(endereco);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/enderecos/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public String update(@RequestBody Endereco endereco, @PathVariable Long id) throws Exception {
		enderecoService.update(endereco, id);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/enderecos/{id}", method = RequestMethod.DELETE)
	public String destroy(@PathVariable Long id) {
		enderecoService.destroy(id);
		return HttpStatus.OK.toString();
	}

	/**
	 * EXCEPTION HANDLER
	 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

}
