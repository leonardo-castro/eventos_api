package com.eventos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.entity.Evento;
import com.eventos.model.service.EventoService;

@RestController
public class EventosController {

	@Autowired
	private EventoService eventoService;

	/**
	 * CRUD OPERATIONS
	 */
	@RequestMapping(value = "/eventos/{id}", method = RequestMethod.GET, produces = "application/json")
	public Evento show(@PathVariable Long id) throws Exception {
		return eventoService.show(id);
	}

	@RequestMapping(value = "/eventos", method = RequestMethod.GET, produces = "application/json")
	public List<Evento> index() {
		return eventoService.index();
	}

	@RequestMapping(value = "/eventos", method = RequestMethod.POST, consumes = "application/json")
	public String create(@RequestBody Evento evento) throws Exception {
		eventoService.create(evento);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/eventos/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public String update(@RequestBody Evento evento, @PathVariable Long id) throws Exception {
		eventoService.update(evento, id);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/eventos/{id}", method = RequestMethod.DELETE)
	public String destroy(@PathVariable Long id) {
		eventoService.destroy(id);
		return HttpStatus.OK.toString();
	}

	/**
	 * EXCEPTION HANDLER
	 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

}
