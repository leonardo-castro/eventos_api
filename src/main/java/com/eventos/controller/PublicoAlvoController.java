package com.eventos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.entity.PublicoAlvo;
import com.eventos.model.service.PublicoAlvoService;

@RestController
public class PublicoAlvoController {

	@Autowired
	private PublicoAlvoService publicoAlvoService;

	/**
	 * CRUD OPERATIONS
	 */
	@RequestMapping(value = "/publico_alvo/{id}", method = RequestMethod.GET, produces = "application/json")
	public PublicoAlvo show(@PathVariable Long id) throws Exception {
		return publicoAlvoService.show(id);
	}

	@RequestMapping(value = "/publico_alvo", method = RequestMethod.GET, produces = "application/json")
	public List<PublicoAlvo> index() {
		return publicoAlvoService.index();
	}

	@RequestMapping(value = "/publico_alvo", method = RequestMethod.POST, consumes = "application/json")
	public String create(@RequestBody PublicoAlvo publicoAlvo) throws Exception {
		publicoAlvoService.create(publicoAlvo);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/publico_alvo/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public String update(@RequestBody PublicoAlvo publicoAlvo, @PathVariable Long id) throws Exception {
		publicoAlvoService.update(publicoAlvo, id);
		return HttpStatus.OK.toString();
	}

	@RequestMapping(value = "/publico_alvo/{id}", method = RequestMethod.DELETE)
	public String destroy(@PathVariable Long id) {
		publicoAlvoService.destroy(id);
		return HttpStatus.OK.toString();
	}

	/**
	 * EXCEPTION HANDLER
	 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

}
