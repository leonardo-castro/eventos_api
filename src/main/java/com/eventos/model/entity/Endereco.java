package com.eventos.model.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "endereco")
public class Endereco implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1812461592334550469L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "endereco_sequence")
	@SequenceGenerator(name = "endereco_sequence", sequenceName = "ENDERECO_SEQUENCE", allocationSize = 1, initialValue = 0)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "rua", nullable = false)
	private String rua;

	@Column(name = "bairro", nullable = false)
	private String bairro;

	// String pois pode conter letras quando é número de apartamento, ex: 43B
	@Column(name = "numero", nullable = true)
	private String numero;

	@Column(name = "cep", nullable = true)
	private String cep;

	@JsonIgnore
	//	@JsonManagedReference(value = "enderecoEvento") // Evita looping infinito de desserialização
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "endereco")
	private Set<Evento> eventos;

	@JsonIgnore
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-2")
	@Column(name = "data_criacao", nullable = false, updatable = false, insertable = true)
	private Calendar dataCriacao;

	@JsonIgnore
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-2")
	@Column(name = "data_alteracao", nullable = true, updatable = true, insertable = true)
	private Calendar dataAlteracao;

	/**
	 * GETTERS & SETTERS
	 * */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Set<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(Set<Evento> eventos) {
		this.eventos = eventos;
	}

	public Calendar getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Calendar getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Calendar dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

}
