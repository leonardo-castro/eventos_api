package com.eventos.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "evento")
public class Evento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3908376909382705709L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evento_sequence")
	@SequenceGenerator(name = "evento_sequence", sequenceName = "EVENTO_SEQUENCE", allocationSize = 1, initialValue = 0)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "titulo", nullable = false)
	private String titulo;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	@Column(name = "imagem_de_capa", nullable = false)
	private String imagemDeCapa;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-2")
	@Column(name = "data", nullable = false, updatable = true, insertable = true)
	private Calendar data;

	// HIBERNATE JOIN
	//	@JsonBackReference(value = "cidadeEvento") // Evita looping infinito de desserialização
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cidade_id", nullable = false)
	private Cidade cidade;

	// HIBERNATE JOIN
	//	@JsonBackReference(value = "enderecoEvento") // Evita looping infinito de desserialização
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "endereco_id", nullable = false)
	private Endereco endereco;

	// HIBERNATE JOIN
	//	@JsonBackReference(value = "publicoAlvoEvento") // Evita looping infinito de desserialização
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "publico_alvo_id", nullable = false)
	private PublicoAlvo publicoAlvo;

	@Column(name = "idade_publico_alvo", nullable = false)
	private Integer idadePublicoAlvo;

	//	@Column(name = "telefone", nullable = false)
	//	private String telefone;
	//
	//	@Column(name = "email", nullable = false)
	//	private String email;

	@Column(name = "valor_da_entrada", nullable = false)
	private BigDecimal valorDaEntrada;

	@Column(name = "valor_media_de_gasto", nullable = false)
	private BigDecimal valorMediaDeGasto;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-2")
	@Column(name = "data_criacao", nullable = false, updatable = false, insertable = true)
	private Calendar dataCriacao;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-2")
	@Column(name = "data_alteracao", nullable = true, updatable = true, insertable = true)
	private Calendar dataAlteracao;

	/**
	 * GETTERS & SETTERS
	 * */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagemDeCapa() {
		return imagemDeCapa;
	}

	public void setImagemDeCapa(String imagemDeCapa) {
		this.imagemDeCapa = imagemDeCapa;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public PublicoAlvo getPublicoAlvo() {
		return publicoAlvo;
	}

	public void setPublicoAlvo(PublicoAlvo publicoAlvo) {
		this.publicoAlvo = publicoAlvo;
	}

	public Integer getIdadePublicoAlvo() {
		return idadePublicoAlvo;
	}

	public void setIdadePublicoAlvo(Integer idadePublicoAlvo) {
		this.idadePublicoAlvo = idadePublicoAlvo;
	}

	public BigDecimal getValorDaEntrada() {
		return valorDaEntrada;
	}

	public void setValorDaEntrada(BigDecimal valorDaEntrada) {
		this.valorDaEntrada = valorDaEntrada;
	}

	public BigDecimal getValorMediaDeGasto() {
		return valorMediaDeGasto;
	}

	public void setValorMediaDeGasto(BigDecimal valorMediaDeGasto) {
		this.valorMediaDeGasto = valorMediaDeGasto;
	}

	public Calendar getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Calendar getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Calendar dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

}
