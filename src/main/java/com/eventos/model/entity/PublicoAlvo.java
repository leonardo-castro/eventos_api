package com.eventos.model.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "publico_alvo")
public class PublicoAlvo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5941128747068909806L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "publico_alvo_sequence")
	@SequenceGenerator(name = "publico_alvo_sequence", sequenceName = "PUBLICO_ALVO_SEQUENCE", allocationSize = 1, initialValue = 0)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "tipo", nullable = false)
	private String tipo;

	@JsonIgnore
	//	@JsonManagedReference(value = "publicoAlvoEvento") // Evita looping infinito de desserialização
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "publicoAlvo")
	private Set<Evento> eventos;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-2")
	@Column(name = "data_criacao", nullable = false, updatable = false, insertable = true)
	private Calendar dataCriacao;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-2")
	@Column(name = "data_alteracao", nullable = true, updatable = true, insertable = true)
	private Calendar dataAlteracao;

	/**
	 * GETTERS & SETTERS
	 * */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Set<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(Set<Evento> eventos) {
		this.eventos = eventos;
	}

	public Calendar getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Calendar getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Calendar dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

}
