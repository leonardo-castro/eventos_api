package com.eventos.model.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.eventos.model.entity.Cidade;

@Transactional
@Repository("cidadeRepository")
@RestResource(exported = false)
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	/**
	 * @RestResource(exported = false)
	 * Evita que o Spring crie endpoints para meus repositórios, ou seja, quando precisar me referir a cidade em um JSON, vou poder utilizar a sintaxe:
	 * "cidade": {
	 * 		"id": 1
	 * }
	 * 
	 * Ao invés de um ENDPOINT como:
	 * "cidade": "/1"
	 * 
	 * Que loucura, não?
	 * */

}
