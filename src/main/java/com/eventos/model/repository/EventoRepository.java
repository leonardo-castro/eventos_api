package com.eventos.model.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.eventos.model.entity.Evento;

@Transactional
@Repository("eventoRepository")
@RestResource(exported = false)
public interface EventoRepository extends JpaRepository<Evento, Long> {

}
