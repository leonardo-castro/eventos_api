package com.eventos.model.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.eventos.model.entity.PublicoAlvo;

@Transactional
@Repository("publicoAlvoRepository")
@RestResource(exported = false)
public interface PublicoAlvoRepository extends JpaRepository<PublicoAlvo, Long> {

}
