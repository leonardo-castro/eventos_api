package com.eventos.model.service;

import java.util.List;

import com.eventos.model.entity.Cidade;

public interface CidadeService {

	/**
	 * CRUD OPERATIONS
	 */
	public Cidade show(Long id);

	public List<Cidade> index();

	public void create(Cidade cidade) throws Exception;

	public void update(Cidade cidade, Long id) throws Exception;

	public void destroy(Long id);

}
