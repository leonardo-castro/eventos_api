package com.eventos.model.service;

import java.util.List;

import com.eventos.model.entity.Endereco;

public interface EnderecoService {

	/**
	 * CRUD OPERATIONS
	 */
	public Endereco show(Long id);

	public List<Endereco> index();

	public void create(Endereco endereco) throws Exception;

	public void update(Endereco endereco, Long id) throws Exception;

	public void destroy(Long id);

}
