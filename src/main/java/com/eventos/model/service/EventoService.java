package com.eventos.model.service;

import java.util.List;

import com.eventos.model.entity.Evento;

public interface EventoService {

	/**
	 * CRUD OPERATIONS
	 */
	public Evento show(Long id);

	public List<Evento> index();

	public void create(Evento evento) throws Exception;

	public void update(Evento evento, Long id) throws Exception;

	public void destroy(Long id);

}
