package com.eventos.model.service;

import java.util.List;

import com.eventos.model.entity.PublicoAlvo;

public interface PublicoAlvoService {

	/**
	 * CRUD OPERATIONS
	 */
	public PublicoAlvo show(Long id);

	public List<PublicoAlvo> index();

	public void create(PublicoAlvo publicoAlvo) throws Exception;

	public void update(PublicoAlvo publicoAlvo, Long id) throws Exception;

	public void destroy(Long id);

}
