package com.eventos.model.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eventos.model.entity.Cidade;
import com.eventos.model.repository.CidadeRepository;
import com.eventos.model.service.CidadeService;
import com.eventos.model.validator.impl.CidadeValidator;

@Service("cidadeService")
public class CidadeServiceImpl implements CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;

	@Override
	public Cidade show(Long id) {
		return cidadeRepository.findOne(id);
	}

	@Override
	public List<Cidade> index() {
		return cidadeRepository.findAll();
	}

	@Override
	public void create(Cidade cidade) throws Exception {
		CidadeValidator cidadeValidator = new CidadeValidator();

		if (cidadeValidator.isValidForCreation(cidade)) {
			cidade.setDataCriacao(Calendar.getInstance());
			cidadeRepository.save(cidade);
		} else {
			throw new Exception(cidadeValidator.getException());
		}
	}

	@Override
	public void update(Cidade cidade, Long id) throws Exception {
		cidade.setId(id);

		CidadeValidator cidadeValidator = new CidadeValidator();
		if (cidadeValidator.isValidForUpdate(cidade)) {
			cidade.setDataAlteracao(Calendar.getInstance());
			cidadeRepository.save(cidade);
		} else {
			throw new Exception(cidadeValidator.getException());
		}
	}

	@Override
	public void destroy(Long id) {
		cidadeRepository.delete(id);
	}

}