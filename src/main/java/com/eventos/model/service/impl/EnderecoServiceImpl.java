package com.eventos.model.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eventos.model.entity.Endereco;
import com.eventos.model.repository.EnderecoRepository;
import com.eventos.model.service.EnderecoService;
import com.eventos.model.validator.impl.EnderecoValidator;

@Service("enderecoService")
public class EnderecoServiceImpl implements EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;

	@Override
	public Endereco show(Long id) {
		return enderecoRepository.findOne(id);
	}

	@Override
	public List<Endereco> index() {
		return enderecoRepository.findAll();
	}

	@Override
	public void create(Endereco endereco) throws Exception {
		EnderecoValidator enderecoValidator = new EnderecoValidator();

		if (enderecoValidator.isValidForCreation(endereco)) {
			endereco.setDataCriacao(Calendar.getInstance());
			enderecoRepository.save(endereco);
		} else {
			throw new Exception(enderecoValidator.getException());
		}
	}

	@Override
	public void update(Endereco endereco, Long id) throws Exception {
		endereco.setId(id);

		EnderecoValidator enderecoValidator = new EnderecoValidator();
		if (enderecoValidator.isValidForUpdate(endereco)) {
			endereco.setDataAlteracao(Calendar.getInstance());
			enderecoRepository.save(endereco);
		} else {
			throw new Exception(enderecoValidator.getException());
		}
	}

	@Override
	public void destroy(Long id) {
		enderecoRepository.delete(id);
	}

}