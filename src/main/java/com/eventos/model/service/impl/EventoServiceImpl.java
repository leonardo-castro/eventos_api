package com.eventos.model.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eventos.model.entity.Evento;
import com.eventos.model.repository.EventoRepository;
import com.eventos.model.service.EventoService;
import com.eventos.model.validator.impl.EventoValidator;

@Service("eventoService")
public class EventoServiceImpl implements EventoService {

	@Autowired
	private EventoRepository eventoRepository;

	@Override
	public Evento show(Long id) {
		return eventoRepository.findOne(id);
	}

	@Override
	public List<Evento> index() {
		return eventoRepository.findAll();
	}

	@Override
	public void create(Evento evento) throws Exception {
		EventoValidator eventoValidator = new EventoValidator();

		if (eventoValidator.isValidForCreation(evento)) {
			evento.setDataCriacao(Calendar.getInstance());
			eventoRepository.save(evento);
		} else {
			throw new Exception(eventoValidator.getException());
		}
	}

	@Override
	public void update(Evento evento, Long id) throws Exception {
		evento.setId(id);

		EventoValidator eventoValidator = new EventoValidator();
		if (eventoValidator.isValidForUpdate(evento)) {
			evento.setDataAlteracao(Calendar.getInstance());
			eventoRepository.save(evento);
		} else {
			throw new Exception(eventoValidator.getException());
		}
	}

	@Override
	public void destroy(Long id) {
		eventoRepository.delete(id);
	}

}