package com.eventos.model.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eventos.model.entity.PublicoAlvo;
import com.eventos.model.repository.PublicoAlvoRepository;
import com.eventos.model.service.PublicoAlvoService;
import com.eventos.model.validator.impl.PublicoAlvoValidator;

@Service("publicoAlvoService")
public class PublicoAlvoServiceImpl implements PublicoAlvoService {

	@Autowired
	private PublicoAlvoRepository publicoAlvoRepository;

	@Override
	public PublicoAlvo show(Long id) {
		return publicoAlvoRepository.findOne(id);
	}

	@Override
	public List<PublicoAlvo> index() {
		return publicoAlvoRepository.findAll();
	}

	@Override
	public void create(PublicoAlvo publicoAlvo) throws Exception {
		PublicoAlvoValidator publicoAlvoValidator = new PublicoAlvoValidator();

		if (publicoAlvoValidator.isValidForCreation(publicoAlvo)) {
			publicoAlvo.setDataCriacao(Calendar.getInstance());
			publicoAlvoRepository.save(publicoAlvo);
		} else {
			throw new Exception(publicoAlvoValidator.getException());
		}
	}

	@Override
	public void update(PublicoAlvo publicoAlvo, Long id) throws Exception {
		publicoAlvo.setId(id);

		PublicoAlvoValidator publicoAlvoValidator = new PublicoAlvoValidator();
		if (publicoAlvoValidator.isValidForUpdate(publicoAlvo)) {
			publicoAlvo.setDataAlteracao(Calendar.getInstance());
			publicoAlvoRepository.save(publicoAlvo);
		} else {
			throw new Exception(publicoAlvoValidator.getException());
		}
	}

	@Override
	public void destroy(Long id) {
		publicoAlvoRepository.delete(id);
	}

}