package com.eventos.model.validator;

public interface Validator<T> {

	public String getException();

	public boolean hasNoErrors(StringBuilder exceptions);

	public boolean isValidForCreation(T object);

	public boolean isValidForUpdate(T object);

}
