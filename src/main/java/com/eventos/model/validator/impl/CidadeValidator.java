package com.eventos.model.validator.impl;

import org.apache.commons.lang3.StringUtils;

import com.eventos.model.entity.Cidade;
import com.eventos.model.validator.Validator;

public class CidadeValidator implements Validator<Cidade> {

	//	@Autowired
	//	private ProdutoService produtoService;

	private String exception;

	@Override
	public String getException() {
		return exception;
	}

	@Override
	public boolean hasNoErrors(StringBuilder exceptions) {
		return StringUtils.isEmpty(exceptions.toString());
	}

	@Override
	public boolean isValidForCreation(Cidade cidade) {
		StringBuilder exceptions = new StringBuilder();

		cidadeValidations(cidade, exceptions);

		//		if (cidade.getDataCriacao() == null) {
		//			exceptions.append("'DataCriacao' não pode ser nulo.\r\n");
		//		}

		if (hasNoErrors(exceptions)) {
			return true;
		}

		this.exception = exceptions.toString();
		return false;
	}

	@Override
	public boolean isValidForUpdate(Cidade cidade) {
		StringBuilder exceptions = new StringBuilder();

		cidadeValidations(cidade, exceptions);

		//		if (cidade.getDataAlteracao() == null) {
		//			exceptions.append("'DataAlteracao' não pode ser nulo.\r\n");
		//		}

		if (hasNoErrors(exceptions)) {
			return true;
		}

		this.exception = exceptions.toString();
		return false;
	}

	private void cidadeValidations(Cidade cidade, StringBuilder exceptions) {
		//		if (StringUtils.isEmpty(categoria.getNome())) {
		//			exceptions.append("'Nome' não pode ser vazio.\r\n");
		//		}
		//
		//		// Uma categoria precisa obrigatóriamente estar atrelada à um produto?
		//		if (CollectionUtils.isEmpty(categoria.getProdutos()) == false) {
		//
		//			// Verifica se produto informado à ser atrelado existe na base de dados
		//			for (Produto produto : categoria.getProdutos()) {
		//				if (produto.getId() == null) {
		//					exceptions.append("'Produto.id' atrelada à categoria não pode ser nulo.");
		//				} else if ((produtoService.show(produto.getId()) == null)) {
		//					exceptions.append("'Produto' informado de id '" + produto.getId().toString()
		//							+ "' atrelado à categoria não existe na base de dados.\r\n");
		//				}
		//			}
		//
		//		}
	}

}
